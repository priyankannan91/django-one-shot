from django.contrib import admin
from .models import TodosList, TodoItem


# Register your models here.
@admin.register(TodosList)
class TodosListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
        "id",
    )
