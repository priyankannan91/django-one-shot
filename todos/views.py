from django.shortcuts import render, get_object_or_404, redirect
from .models import TodosList, TodoItem
from .forms import TodoListForm, TodoItemForm


# Create your views here.
# Task List Data
def todos_list_list(request):
    todos = TodosList.objects.all()
    context = {
        "todos_list": todos,
    }
    return render(request, "todos/list.html", context)


# Sub Task List Data
def todo_list_detail(request, id):
    todo = get_object_or_404(TodosList, id=id)
    context = {
        "todo_detail": todo,
    }
    return render(request, "todos/details.html", context)


# TodosList - Create, validate, submit and redirect to created list
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


# TodosList - Edit todo list
def todo_list_update(request, id):
    # get the entry associated with specified id store in 'todos' (same name as list recipe) object
    todo = TodosList.objects.get(id=id)
    # if the request is submitted
    if request.method == "POST":
        # store the submitted instance in 'form' object
        form = TodoListForm(request.POST, instance=todo)
        # if form is valid
        if form.is_valid():
            # save form to 'todos' object
            todo = form.save()
            # redirect user to the subtask page you just updated
            return redirect("todo_list_detail", id=id)
    # If request has not been submitted redirect user to a clean instance of form
    else:
        form = TodoListForm(instance=todo)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


# TodosList - Delete sublist on request
def todo_list_delete(request, id):
    todo = TodosList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        # redirect to list page
        return redirect("todos_list_list")
    return render(request, "todos/delete.html")


# TodoItem - Create, validate, submit and redirect to created item
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


# TodoItem - Edit an existing todo item and redirect to "todo_list_detail"
def todo_item_update(request, id):
    todo = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            # todo_list_detail id cannot be same as todo_item_update id
            # How do I redirect to the correct id
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm(instance=todo)

    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)


# Pseudo for creating basic list view
# define function, parameter is 'request'
# if the request has been created
#   save request in form
#   if form is valid
#       save form in new object
#       redirect to page details page using the object id
# else create a new instance of form
# create context to hold form
# render webpage and return
