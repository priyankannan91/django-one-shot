from django.forms import ModelForm
from .models import TodosList, TodoItem


class TodoListForm(ModelForm):
    class Meta:
        # inherit from model class TodosList
        model = TodosList
        # name field will populate on "create/"
        fields = [
            "name",
        ]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
