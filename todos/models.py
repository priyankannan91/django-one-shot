from django.db import models


# Create your models here.
# Task Properties
class TodosList(models.Model):
    name = models.CharField(max_length=100)
    create_on = models.DateField(auto_now_add=True)
    # Djano auto creates an 'id' here

    # How does this update Recipe (under Recipe steps in admin) to populate with title
    def __str__(self):
        return self.name


# Sub Task Properties
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)

    # Get access to TodosList fields through this foreignkey
    list = models.ForeignKey(
        TodosList,
        related_name="items",
        on_delete=models.CASCADE,
    )
